package com.clean.coder.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
/**
 * @author kamu kamlesh
 *
 */
@RestController
@RequestMapping("services/onboard/")
@CrossOrigin(origins = "*")
public class OnboardController {

    /**
     * used to on-board the agent
     * @return n
     */
    @GetMapping("agent")
    public String onboardAgent() {
        return "Hi, Onboard the agent.....";
    }

}
