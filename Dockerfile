FROM openjdk:8
EXPOSE 8090
ADD target/onboard-service.war onboard-service.war
ENTRYPOINT ["java","-jar","/onboard-service.war"]